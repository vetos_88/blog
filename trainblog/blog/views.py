from django.shortcuts import render
from django.template import loader, Context
from django.http import HttpResponse
from blog.models import BlogPost


def archive(request):
    posts = BlogPost.objects.all()
    templt = loader.get_template("blog/archive.html")
    cont = Context({'posts': posts})
    return HttpResponse(templt.render(cont))

