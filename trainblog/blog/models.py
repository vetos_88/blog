from django.db import models as m
from django.contrib import admin


class BlogPost(m.Model):
    title = m.CharField(max_length=150)
    body = m.TextField()
    timestamp = m.DateField()

    class Meta:
        ordering = ('-timestamp',)


class BlogPostAdmin(admin.ModelAdmin):
    list_display = ('title', 'timestamp')

admin.site.register(BlogPost, BlogPostAdmin)

